package com.geodox.slashplayed;

import com.geodox.slashplayed.command.SlashPlayedCommand;
import com.geodox.slashplayed.data.EventPassThrough;
import com.geodox.slashplayed.res.ModInfo;
import net.minecraft.command.ServerCommandManager;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;

@Mod(modid = ModInfo.MOD_ID, version = ModInfo.VERSION)
public class SlashPlayed
{
    @EventHandler
    public void preInitialize(FMLPreInitializationEvent event)
    {
        MinecraftForge.EVENT_BUS.register(EventPassThrough.instance);
        ClientCommandHandler.instance.registerCommand(new SlashPlayedCommand());
    }

    @EventHandler
    public void serverStart(FMLServerStartingEvent event)
    {
        MinecraftServer minecraftServer = event.getServer();
        ServerCommandManager serverCommandManager = (ServerCommandManager) minecraftServer.getCommandManager();

        serverCommandManager.registerCommand(new SlashPlayedCommand());
    }
}
