package com.geodox.slashplayed.helper;

/**
 * Created by GeoDoX on 2015-11-10.
 */
public class TimeCalc
{
    public static String calcTicksToTime(long ticks)
    {
        // Store the ticks in each appropriate variable, doing the correct math
        int seconds = (int) ticks / 20;
        int minutes = seconds / 60;
        int hours = minutes / 60;
        int days = hours / 24;

        // Take away the time that was accumulated in each variable, doing the correct math
        seconds -= minutes * 60;
        minutes -= hours * 60;
        hours -= days * 24;

        // Return the appropriate string that prints only what is necessary
        return timeToString(days, hours, minutes, seconds);
    }

    public static String calcMillisToTime(long millis)
    {
        // Store the ticks in each appropriate variable, doing the correct math
        int seconds = Math.round(millis / 1000);
        int minutes = seconds / 60;
        int hours = minutes / 60;
        int days = hours / 24;

        // Take away the time that was accumulated in each variable, doing the correct math
        seconds -= minutes * 60;
        minutes -= hours * 60;
        hours -= days * 24;

        // Return the appropriate string that prints only what is necessary
        return timeToString(days, hours, minutes, seconds);
    }

    private static String timeToString(int days, int hours, int minutes, int seconds)
    {
        if(days > 0)
            return days + " days, " + hours + " hours, " + minutes + " minutes, and " + seconds + " seconds.";
        if(hours > 0)
            return hours + " hours, " + minutes + " minutes, and " + seconds + " seconds.";
        else if(minutes > 0)
            return minutes + " minutes, and " + seconds + " seconds.";
        else
            return seconds + " seconds.";
    }
}
