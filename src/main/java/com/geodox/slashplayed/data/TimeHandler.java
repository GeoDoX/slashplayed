package com.geodox.slashplayed.data;

import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagLong;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

/**
 * Created by GeoDoX on 2015-11-10.
 */
public class TimeHandler
{
    public static final TimeHandler instance = new TimeHandler();

    //private long firstTimeInServer;
    private long timeInServer;
    private long sessionTimeInServer;
    public long getTimeInServer()
    {
        return timeInServer;
    }
    public long getSessionTimeInServer()
    {
        return sessionTimeInServer;
    }

    public void onPlayerLogIn(PlayerEvent.PlayerLoggedInEvent event)
    {
        NBTTagCompound playerNBT = event.player.getEntityData();

        // Time in Server
        NBTBase worldTimeNBT = playerNBT.getTag("timeInServer");
        if(worldTimeNBT != null)
        {
            timeInServer = ((NBTTagLong) worldTimeNBT).getLong();
            sessionTimeInServer = 0;
            //System.out.println(timeInServer);
        }
        else
            timeInServer = 0;
    }

    public void onPlayerLogOut(PlayerEvent.PlayerLoggedOutEvent event)
    {
        NBTTagCompound playerNBT = event.player.getEntityData();

        playerNBT.setLong("timeInServer", timeInServer);
    }

    public void onWorldTick(TickEvent.WorldTickEvent event, long calculatedTimePassed)
    {
        timeInServer += calculatedTimePassed;
        sessionTimeInServer += calculatedTimePassed;
    }

    public void reset()
    {
        timeInServer = 0;
        sessionTimeInServer = 0;
    }
}
