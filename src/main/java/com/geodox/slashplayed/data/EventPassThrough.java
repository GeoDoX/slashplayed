package com.geodox.slashplayed.data;

import net.minecraftforge.event.entity.player.PlayerPickupXpEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

/**
 * Created by GeoDoX on 2015-11-10.
 */
public class EventPassThrough
{
    public static final EventPassThrough instance = new EventPassThrough();

    private long nowTime;
    private long lastTime;
    private long calculatedTimePassed;

    @SubscribeEvent
    public void onPlayerLogIn(PlayerEvent.PlayerLoggedInEvent event)
    {
        TimeHandler.instance.onPlayerLogIn(event);
        XPHandler.instance.onPlayerLogIn(event);

        lastTime = System.currentTimeMillis();
    }

    @SubscribeEvent
    public void onPlayerPickupXP(PlayerPickupXpEvent event)
    {
        XPHandler.instance.onXPPickUp(event);
    }

    @SubscribeEvent
    public void onPlayerLogOut(PlayerEvent.PlayerLoggedOutEvent event)
    {
        TimeHandler.instance.onPlayerLogOut(event);
        XPHandler.instance.onPlayerLogOut(event);
    }

    @SubscribeEvent
    public void onWorldTick(TickEvent.WorldTickEvent event)
    {
        nowTime = System.currentTimeMillis();
        calculatedTimePassed = (nowTime - lastTime);
        TimeHandler.instance.onWorldTick(event, calculatedTimePassed);
        XPHandler.instance.onWorldTick(event, calculatedTimePassed);
        lastTime = nowTime;
    }
}
