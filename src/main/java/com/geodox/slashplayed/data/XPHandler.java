package com.geodox.slashplayed.data;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagInt;
import net.minecraft.nbt.NBTTagLong;
import net.minecraftforge.event.entity.player.PlayerPickupXpEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

/**
 * Created by GeoDoX on 2015-11-10.
 */
public class XPHandler
{
    public static final XPHandler instance = new XPHandler();

    private String NBTXPLevel = "xpLevel";
    private String NBTTimeAtLevel = "timeAtXPLevel";

    private int xpLevel;
    private float currentXP;
    private float remainingXP;
    private long timeAtXPLevel;
    private long estimatedTimeToNextLevel;
    private long lastEstimatedTimeToNextLevel;
    private long averageEstimatedTimeToNextLevel;
    public long getTimeAtCurrentLevel()
    {
        return timeAtXPLevel;
    }
    public long getEstimatedTimeToNextLevel()
    {
        return averageEstimatedTimeToNextLevel;
    }

    public void onPlayerLogIn(PlayerEvent.PlayerLoggedInEvent event)
    {
        // Get the NBT of the Player
        NBTTagCompound playerNBT = event.player.getEntityData();

        // Experience Level
        NBTBase nbtXPLevel = playerNBT.getTag(NBTXPLevel);
        if(nbtXPLevel != null)
        {
            xpLevel = ((NBTTagInt) nbtXPLevel).getInt();
            // Time At Experience Level
            NBTBase nbtTimeAtLevel = playerNBT.getTag(NBTTimeAtLevel);
            if(nbtTimeAtLevel != null)
            {
                timeAtXPLevel = ((NBTTagLong) nbtTimeAtLevel).getLong();
            }
            else
                timeAtXPLevel = 0;
        }
        else
            xpLevel = event.player.experienceLevel;
    }

    public void onXPPickUp(PlayerPickupXpEvent event)
    {
        if(event.entity instanceof EntityPlayer)
        {
            EntityPlayer player = (EntityPlayer) event.entity;

            currentXP = player.experience;
            remainingXP = player.experienceTotal - currentXP;

            if (xpLevel != player.experienceLevel)
            {
                xpLevel = player.experienceLevel;
                timeAtXPLevel = 0;
            }
        }
    }

    public void onPlayerLogOut(PlayerEvent.PlayerLoggedOutEvent event)
    {
        NBTTagCompound playerNBT = event.player.getEntityData();

        playerNBT.setInteger(NBTXPLevel, xpLevel);
        playerNBT.setLong(NBTTimeAtLevel, timeAtXPLevel);
    }

    public void onWorldTick(TickEvent.WorldTickEvent event, long calculatedTimePassed)
    {
        timeAtXPLevel += calculatedTimePassed;

        lastEstimatedTimeToNextLevel = estimatedTimeToNextLevel;
        estimatedTimeToNextLevel = (long) ((timeAtXPLevel * currentXP) / remainingXP);
        averageEstimatedTimeToNextLevel *= 0.9 + estimatedTimeToNextLevel * 0.1;
    }

    public void reset()
    {
        timeAtXPLevel = 0;
    }
}
