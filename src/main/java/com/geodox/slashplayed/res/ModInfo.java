package com.geodox.slashplayed.res;

/**
 * Created by GeoDoX on 2015-11-07.
 */
public class ModInfo
{
    public static final String MOD_ID = "slashPlayed";
    public static final String VERSION = "1.4.1";
}

// Features:
// Total World Time
// Total Play Time
// Current Play Time this Session
// Time at Current Level
// Estimated Time to Next Level (time remaining this level) = (time this level)*(exp remaining this level)/(exp got this level so far)
