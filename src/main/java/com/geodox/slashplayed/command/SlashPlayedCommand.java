package com.geodox.slashplayed.command;

import com.geodox.slashplayed.data.TimeHandler;
import com.geodox.slashplayed.data.XPHandler;
import com.geodox.slashplayed.helper.TimeCalc;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.BlockPos;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.World;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GeoDoX on 2015-11-07.
 */
public class SlashPlayedCommand extends CommandBase
{
    private final List<String> aliases;

    private final List<String> validArgs;
    private static final String worldTimeArg = "worldTime";
    private static final String totalPlayTimeArg = "totalPlayTime";
    private static final String currentSessionTimeArg = "currentSessionTime";
    private static final String timeAtLevelArg = "timeAtLevel";
    private static final String estimatedToNextLevelArg = "estimatedToNext";
    private static final String resetAll = "resetAll";

    public SlashPlayedCommand()
    {
        aliases = new ArrayList<String>();
        aliases.add("played");
        aliases.add("play");

        validArgs = new ArrayList<String>();
        validArgs.add(worldTimeArg);
        validArgs.add(totalPlayTimeArg);
        validArgs.add(currentSessionTimeArg);
        validArgs.add(timeAtLevelArg);
        validArgs.add(estimatedToNextLevelArg);
        validArgs.add(resetAll);
    }

    @Override
    public String getCommandName()
    {
        return "played";
    }

    @Override
    public String getCommandUsage(ICommandSender sender)
    {
        return "played <text>";
    }

    @Override
    public List getCommandAliases()
    {
        return this.aliases;
    }

    @Override
    public void processCommand(ICommandSender sender, String[] args) throws CommandException
    {
        World world = sender.getEntityWorld();

        if (sender instanceof EntityPlayer)
        {
            EntityPlayer player = (EntityPlayer) sender;

            // Get the totalWorldTime
            long totalWorldTime = world.getTotalWorldTime();

            if (args.length == 0)
            {
                // Print the time in Chat
                player.addChatMessage(new ChatComponentText("Total Play Time: " + TimeCalc.calcMillisToTime(TimeHandler.instance.getTimeInServer())));
                player.addChatMessage(new ChatComponentText("Current Session Time: " + TimeCalc.calcMillisToTime(TimeHandler.instance.getSessionTimeInServer())));
                player.addChatMessage(new ChatComponentText("Time At Current Level: " + TimeCalc.calcMillisToTime(XPHandler.instance.getTimeAtCurrentLevel())));
                //player.addChatMessage(new ChatComponentText("Est. Time to Next Level: " + TimeCalc.calcMillisToTime(XPHandler.instance.getEstimatedTimeToNextLevel())));
            }
            else
            {
                for (String arg : args)
                {
                    if (arg.equals(worldTimeArg))
                        player.addChatMessage(new ChatComponentText("Total World Time: " + TimeCalc.calcTicksToTime(totalWorldTime)));
                    else if (arg.equals(totalPlayTimeArg))
                        player.addChatMessage(new ChatComponentText("Total Play Time: " + TimeCalc.calcMillisToTime(TimeHandler.instance.getTimeInServer())));
                    else if (arg.equals(currentSessionTimeArg))
                        player.addChatMessage(new ChatComponentText("Current Session Time: " + TimeCalc.calcMillisToTime(TimeHandler.instance.getSessionTimeInServer())));
                    else if (arg.equals(timeAtLevelArg))
                        player.addChatMessage(new ChatComponentText("Time At Current Level: " + TimeCalc.calcMillisToTime(XPHandler.instance.getTimeAtCurrentLevel())));
                    else if (arg.equals(estimatedToNextLevelArg))
                        player.addChatMessage(new ChatComponentText("Est. Time to Next Level: " + "Not Yet Available"));
                    else if(arg.equals(resetAll))
                    {
                        player.addChatMessage(new ChatComponentText("Resetting Information..."));
                        TimeHandler.instance.reset();
                        XPHandler.instance.reset();
                    }
                    else
                        player.addChatMessage(new ChatComponentText(arg + " is not a valid argument"));
                }
            }
        }
    }

    @Override
    public List<String> addTabCompletionOptions(ICommandSender sender, String[] args, BlockPos pos)
    {
        return validArgs;
    }

    @Override
    public int getRequiredPermissionLevel()
    {
        return 0;
    }
}
